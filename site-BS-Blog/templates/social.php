<?php 
require 'inc/header.php'; 
include('inc/twitteroauth/OAuth.php');
include('inc/twitteroauth/twitteroauth.php');
?>

    <!-- Page Content -->
    <div class="container">

        <div class="row row-centered">
            <?php 
            	if($page->h1){
            		echo '<h1 class="text-left">'.$page->h1.'</h1>';
            	}
            ?>

            <div class="col-lg-6 col-centered text-left lead" id="twitterCol">

                <?php 
                  $outputTweet='';
                  $twitteruser = 'Lazysod';
                  $tweets = getTweets($twitteruser);
                  foreach ($tweets as $line){
                      $status = $line->text;
                      $tweetTime =  $line->created_at;
                      $tweetId = $line->id_str;
                    $Timage = $line->user->profile_image_url;
                    $twitteruser = $line->user->screen_name;
                    $name = $line->user->name;
                    $screen_name = $line->user->screen_name;
                    $about = $line->user->description;
                    //_bigger
                    $Timage = str_replace('_normal', '_bigger', $Timage);
                    $status = preg_replace('%(http://([a-z0-9_.+&!#~/,\-]+))%i','<a href="http://$2" target="_NEW">$1</a>',$status);
                      $status = preg_replace('/@([a-z0-9_]+)/i','<a href="http://twitter.com/$1" target="_NEW">@$1</a>',$status);
                      $status = preg_replace('/(^|\s)#(\w*[a-zA-Z_]+\w*)/', '\1#<a href="http://twitter.com/hashtag/\2?src=hash" target="_NEW">\2</a>', $status);
                      $outputTweet .= '<div class="tweets">

                  <a href="http://twitter.com/'.$twitteruser.'" title="'.$twitteruser.' on Twitter" target="_blank"><img src="'.$Timage.'" align="left" alt="@'.$screen_name.' twitter avatar" class="img-responsive" border="0"> <i class="fa fa-twitter"></i>'.$twitteruser.'</a> <a style="font-size:85%" href="http://twitter.com/'.$twitteruser.'/statuses/'.$tweetId.'">'. twitter_time($tweetTime) .'</a><br/> '.$status.'</span></div>';
                     $name = $line->user->name;
                  }
                  echo $outputTweet;
                ?>
            </div>

            <div class="col-md-4 col-centered" id="leftCol">
                
					<div class="well text-left">
            <div id="sidebar">
              <div class="well text-left">
                <h3 class="text-center">Me on Social Media</h3>
                  <div class="list-group">
                    <!-- start social  -->
                     <?php  
                      if($homepage->facebook){ ?>
                        
                          <a class="list-group-item" href="<?php echo $homepage->facebook; ?>" target="_NEW" title="Join me on Facebook"><i class="fa fa-facebook-square"></i> connect with me on Facebook</a>
                        
                      <?php 
                        }
                        if($homepage->gplus){ ?>
                      <!-- Google plus link -->
                        
                          <a class="list-group-item" href="<?php echo $homepage->gplus; ?>" target="_NEW" title="Circle me on Google Plus"><i class="fa fa-google-plus-square"></i> Circle me on Google Plus</a>
                        
                      <?php 
                        } 
                      if($homepage->twitter){ ?>
                      <!-- Twitter link -->
                      
                        <a class="list-group-item" href="<?php echo $homepage->twitter; ?>" target="_NEW" title="Follow me on Twitter"><i class="fa fa-twitter-square"></i> Follow me on Facebook</a>
                      
                      <?php
                        }
                      if($homepage->instagram){ ?>
                      <!-- instagram link -->
                      
                        <a class="list-group-item" href="<?php echo $homepage->instagram; ?>" target="_NEW" title="Follow me on Instagram"><i class="fa fa-instagram"></i> Follow me on Instagram</a>
                      
                      <?php 
                        }
                        if($homepage->linkedin){ ?>
                      <!-- linkedin link -->
                      
                        <a class="list-group-item" href="<?php echo $homepage->linkedin; ?>" target="_NEW" title="connect to me on Linkedin"><i class="fa fa-linkedin-square"></i> Connect with me on Linkedin</a>
                      
                      <?php } ?>
                    <!-- end social  -->
                  </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->

    </div>
    <?php include_once 'inc/footer.php';?>
    <!-- /.container -->
    <!-- jQuery Version 1.11.1 -->
    <script src="<?php echo $config->urls->templates; ?>/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $config->urls->templates; ?>/js/bootstrap.min.js"></script>

</body>

</html>
