<?php require 'inc/header.php'; ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row row-centered">
            <?php 
            	if($page->h1){
            		echo '<h1 class="text-center">'.$page->greeting.'</h1>';
            	}
            ?>
            <div class="col-lg-8 well text-left">
                
                <!-- start content -->
                <?php 
                	if($page->h1){
                		echo '<h1>'.$page->h1.'</h1>';
                	}

                  echo $page->body;
                  // Start blog
                  // Processwire 
                  $rss = $modules->get("MarkupLoadRSS");
                  $rss->limit = 20;
                  $rss->cache = 0; 
                  $rss->maxLength = 255; 
                  $rss->dateFormat = 'm/d/Y H:i:s';
                  $rss->stripTags = false;

                  $rss->load("http://geeklocker.tumblr.com/rss");

                  echo "<h2>{$rss->title}</h2>";
                  echo "<p>{$rss->description}</p>";
                  echo "<ul>"; 
              function make_links_clickable($text){
                  return preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_NEW">$1</a>', $text);
              }
                  foreach($rss as $item) {
                       echo "<li>" . make_links_clickable($item->title) . "</li>";
                  }

                  echo "</ul>";
                  // end blog
                ?>
            </div>
            <div class="col-md-4" id="leftCol">
                
					<div class="well text-left">

                <a href="<?php echo $homepage->tumblr; ?>" target="_NEW"><h2><i class="fa fa-tumblr-square"></i> Geeklocker on Tumblr</h2></a>
          </div>
             
        </div>
        <!-- /.row -->

    </div>
    <?php include_once 'inc/footer.php';?>
    <!-- /.container -->
    <!-- jQuery Version 1.11.1 -->
    <script src="<?php echo $config->urls->templates; ?>/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $config->urls->templates; ?>/js/bootstrap.min.js"></script>

</body>

</html>
