<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		
		<?php
			$homepage = $pages->get(1); 
			//echo $homepage->siteTitle;
			
			if($page->siteTitle == ''){
				
				$title = $homepage->siteTitle;
			}else{
				$title = $page->siteTitle;
			}
			/**/ 
			if($page->seoDescription == ''){
				$desc = $homepage->seoDescription;
			}else{
				$desc = $page->seoDescription;
			}
			//echo $title.' - '.$desc;
			
		?>

		<title><?php echo $title.' - '.$desc; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="<?php echo $config->urls->templates; ?>css/bootstrap.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="<?php echo $config->urls->templates; ?>css/custom.css" rel="stylesheet">
		
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

		<?php if($page->parent_id==1021){ ?>
		<!-- start gallery --> 
		<!-- Add jQuery library -->
		<script type="text/javascript" src="<?php echo $config->urls->templates; ?>fancybox/lib/jquery-1.10.1.min.js"></script>

		<!-- Add mousewheel plugin (this is optional) -->
		<script type="text/javascript" src="<?php echo $config->urls->templates; ?>fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

		<!-- Add fancyBox main JS and CSS files -->
		<script type="text/javascript" src="<?php echo $config->urls->templates; ?>fancybox/jquery.fancybox.js?v=2.1.5"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

		<!-- Add Button helper (this is optional) -->
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
		<script type="text/javascript" src="<?php echo $config->urls->templates; ?>fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

		<!-- Add Thumbnail helper (this is optional) -->
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
		<script type="text/javascript" src="<?php echo $config->urls->templates; ?>fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

		<!-- Add Media helper (this is optional) -->
		<script type="text/javascript" src="<?php echo $config->urls->templates; ?>fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>	
		<!-- end gallery -->

		<script type="text/javascript">
		    $(document).ready(function() {
		        $(".fancybox").fancybox();
		    });
		</script>
		<?php } ?>
		<?php if($page->id=='27') { ?>
			<style>
			.img-responsive {
			    margin: 0 auto;
			}
			</style>
		<?php } ?>

	</head>
	<body>
<nav class="navbar navbar-blue navbar-fixed-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      		<a href="<?php  echo $config->urls->root; ?>" class="navbar-brand"><?php echo $pages->get(1)->siteTitle; ?></a>      
    </div>
	<nav class="collapse navbar-collapse" role="navigation">
	<?php 
	include_once 'nav.php';
	include_once 'functions.php';
	?>
	</nav>
  </div>
</nav>