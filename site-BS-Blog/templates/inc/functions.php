<?php 

/* 
	Additonal functions
*/

function wordLimiter($str = '', $limit = 400, $endstr = '...'){

    if($str == '') return '';

    if(strlen($str) <= $limit) return $str;

    $out = substr($str, 0, $limit);
    $pos = strrpos($out, " ");
    if ($pos>0) {
        $out = substr($out, 0, $pos);
    }
    $out .= $endstr;
    return $out;

}      

// Twitter functions 
function _make_url_clickable_cb($matches) {
  $ret = '';
  $url = $matches[2];
 
  if ( empty($url) )
    return $matches[0];
  // removed trailing [.,;:] from URL
  if ( in_array(substr($url, -1), array('.', ',', ';', ':')) === true ) {
    $ret = substr($url, -1);
    $url = substr($url, 0, strlen($url)-1);
  }
  return $matches[1] . "<a href=\"$url\" target=\"_NEW\">$url</a>" . $ret;
}
 
function _make_web_ftp_clickable_cb($matches) {
  $ret = '';
  $dest = $matches[2];
  $dest = 'http://' . $dest;
 
  if ( empty($dest) )
    return $matches[0];
  // removed trailing [,;:] from URL
  if ( in_array(substr($dest, -1), array('.', ',', ';', ':')) === true ) {
    $ret = substr($dest, -1);
    $dest = substr($dest, 0, strlen($dest)-1);
  }
  return $matches[1] . "<a href=\"$dest\" target=\"_NEW\">$dest</a>" . $ret;
}
 
function _make_email_clickable_cb($matches) {
  $email = $matches[2] . '@' . $matches[3];
  return $matches[1] . "<a href=\"mailto:$email\">$email</a>";
}
 
function make_clickable($ret) {
  $ret = ' ' . $ret;
  // in testing, using arrays here was found to be faster
  $ret = preg_replace_callback('#([\s>])([\w]+?://[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', '_make_url_clickable_cb', $ret);
  $ret = preg_replace_callback('#([\s>])((www|ftp)\.[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', '_make_web_ftp_clickable_cb', $ret);
  $ret = preg_replace_callback('#([\s>])([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})#i', '_make_email_clickable_cb', $ret);
 
  // this one is not in an array because we need it to run last, for cleanup of accidental links within links
  $ret = preg_replace("#(<a( [^>]+?>|>))<a [^>]+?>([^>]+?)</a></a>#i", "$1$3</a>", $ret);
  $ret = trim($ret);
  return $ret;
}

function getTweets($twitteruser) { 
    // Set number of tweets
    $notweets = 1;
    $consumerkey = "jKzuJVecOTRC6MTb35Q9m4ztL";
    $consumersecret = "UWs6zz1PuDFFdUIWI3T2rqZl80Ot4MSZxD3vVHQWNuKP9gb6Ii";
    $accesstoken = "19595974-bBOZqaFWLtwHYUQbpcK6WAbMLanBzMBk6IkwHt6M5";
    $accesstokensecret = "YExO5HHCU2dXQbycyvcCDrYtEbbSCcoJqskZpT8CsfyuT";
      
    function getConnectionWithAccessToken($cons_key, $cons_secret, $oauth_token, $oauth_token_secret) {
      $connection = new TwitterOAuth($cons_key, $cons_secret, $oauth_token, $oauth_token_secret);
      return $connection;
    }
       
    $connection = getConnectionWithAccessToken($consumerkey, $consumersecret, $accesstoken, $accesstokensecret);
    $tweets = $connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=".$twitteruser."&count=".$notweets);
     
    return ($tweets);
}
    // Determin Twitter Time

function twitter_time($a) {
    //get current timestampt
    $b = strtotime("now"); 
    //get timestamp when tweet created
    $c = strtotime($a);
    //get difference
    $d = $b - $c;
    //calculate different time values
    $minute = 60;
    $hour = $minute * 60;
    $day = $hour * 24;
    $week = $day * 7;
        
    if(is_numeric($d) && $d > 0) {
        //if less then 3 seconds
        if($d < 3) return "right now";
        //if less then minute
        if($d < $minute) return floor($d) . " seconds ago";
        //if less then 2 minutes
        if($d < $minute * 2) return "about 1 minute ago";
        //if less then hour
        if($d < $hour) return floor($d / $minute) . " minutes ago";
        //if less then 2 hours
        if($d < $hour * 2) return "about 1 hour ago";
        //if less then day
        if($d < $day) return floor($d / $hour) . " hours ago";
        //if more then day, but less then 2 days
        if($d > $day && $d < $day * 2) return "yesterday";
        //if less then year
        if($d < $day * 365) return floor($d / $day) . " days ago";
        //else return more than a year
        return "over a year ago";
    }
}
function get_hashtags($dbcon, $tweet) {
    $tweet = preg_replace('/(^|\s)@(\w+)/',
    '\1<a href="http://www.twitter.com/\2">@\2</a>',
    $tweet);
    return preg_replace('/(^|\s)#(\w+)/',
    '\1<a href="http://twitter.com.com/hashtag/\2">#\2</a>',
    $tweet);
}
