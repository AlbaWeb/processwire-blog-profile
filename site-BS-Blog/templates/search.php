<?php require 'inc/header.php'; 
    


?>

    <!-- Page Content -->
    <div class="container">

        <div class="row row-centered">
            <?php 
            	if($page->h1){
            		echo '<h1 class="text-center">'.$page->greeting.'</h1>';
            	}
            ?>
            <div class="col-lg-12 col-md-12 well  col-xs-12 col-centered">
                
<?php 
    $out = '';


    // if a branche is specified, then we limit the results to having that branche (branches field in those pages)
    if($input->get->branche) {
        $branche = $pages->get("/branches/" . $sanitizer->pageName($input->get->branche));
        if($branche->id) {
            $selector .= "branches=$branche, ";//CHANGE YOUR SELECTOR AS SHOWN HERE
        $summary["branche"] = $branche->title;
        $input->whitelist('branche', $branche->name);
        }
    }



    if($q = $sanitizer->selectorValue($input->get->q)) {

        // Send our sanitized query 'q' variable to the whitelist where it will be
        // picked up and echoed in the search box by the head.inc file.
        $input->whitelist('q', $q);

        // Search the title, body and sidebar fields for our query text.
        // Limit the results to 50 pages.
        // Exclude results that use the 'admin' template.
        $matches = $pages->find("title|body~=$q, limit=50");
        $count = count($matches);

        if($count) {
            $out .= "<h2>Found $count pages matching your query:</h2>" .
                "<ul class='nav'>";

            foreach($matches as $m) {
                $out .= "<li><p><a href='{$m->url}'>{$m->title}</a><br />{$m->summary}</p></li>";
            }

            $out .= "</ul>";

        } else {
            $out .= "<h2>Sorry, no results were found.</h2>";
        }
    } else {
        $out .= "<h2>This is from my search.php page</h2>";
    }
    echo $out;
?>
                </div>
            </div> 
        </div>
        <!-- /.row -->
		<div class="row row-centered">   
			<footer>
		   		<p class="footer">Copyright <?php echo date('Y'); ?> &copy; <a href="http://twitter.com/lazysod" target="_NEW">Barry Smith</a></p>
			</footer>    
		</div>
    </div>
    <!-- /.container -->
    <!-- jQuery Version 1.11.1 -->
    <script src="<?php echo $config->urls->templates; ?>/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $config->urls->templates; ?>/js/bootstrap.min.js"></script>

</body>

</html>
