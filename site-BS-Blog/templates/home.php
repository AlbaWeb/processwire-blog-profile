<?php 


require 'inc/header.php'; 


?>

    <!-- Page Content -->
    <div class="container">

        <div class="row row-centered">
            <?php 
            	if($page->greeting){
            		echo '<h1 class="text-center">'.$page->greeting.'</h1>';
            	}
            ?>
            <div class="col-lg-12 col-md-12  col-xs-12 col-centered">
                
                <!-- start content -->
                <h3>About me:</h3>
                <div class="col-lg-6 col-md-12 col-xs-12 text-left">
                  
	                <?php 
	                // Feel free to customise this
	                //echo '<pre>'; 
	                echo $page->body;
					//echo '</pre>'; 
					?>
                                       
                </div>
                <div class="col-lg-6 col-md-8 col-xs-12 text-center">
	                <!-- start -->
	                <?php 
	                //$social = new SocialLinks;
	                
	                //echo $social->show('test');
	                
	                ?>
	                <!-- end -->  
	                <!-- image on right -->
                  <?php foreach($page->image as $image){ ?>
	                <img class="circular img-responive" src="<?php echo $image->url; ?>" alt="Picture of me">
	                <!-- Image label -->
	                <p class="lead"><i><?php echo $page->image->description; ?></i></p>
                  <?php } ?>
           			<!-- start social --> 
                  <?php  
                  if($page->facebook){ ?>
                    <div class="col-md-2 largetxt col-centered">
                      <a href="<?php echo $page->facebook; ?>" target="_NEW" title="Join me on Facebook"><i class="fa fa-facebook-square"></i></a>
                    </div>
                  <?php 
                    }
                    if($page->gplus){ ?>
                  <!-- Google plus link -->
                    <div class="col-md-2 largetxt col-centered">
                      <a href="<?php echo $page->gplus; ?>" target="_NEW" title="Circle me on Google Plus"><i class="fa fa-google-plus-square"></i></a>
                    </div>
                  <?php 
                    } 
                  if($page->twitter){ ?>
                  <!-- Twitter link -->
                  <div class="col-md-2 largetxt col-centered">
                    <a href="<?php echo $page->twitter; ?>" target="_NEW" title="Follow me on Twitter"><i class="fa fa-twitter-square"></i></a>
                  </div>
                  <?php
                    }
                  if($page->instagram){ ?>
                  <!-- instagram link -->
                  <div class="col-md-2 largetxt col-centered">
                    <a href="<?php echo $page->instagram; ?>" target="_NEW" title="Follow me on Instagram"><i class="fa fa-instagram"></i></a>
                  </div>
                  <?php 
                    }
                    if($page->linkedin){ ?>
                  <!-- linkedin link -->
                  <div class="col-md-2 largetxt col-centered">
                    <a href="<?php echo $page->linkedin; ?>" target="_NEW" title="connect to me on Linkedin"><i class="fa fa-linkedin-square"></i></a>
                  </div>
                  <?php } ?>
           			<!-- end social --> 
                </div>
            </div> 
        </div>
        <!-- /.row -->
		<div class="row row-centered">   
			<footer>
		   		<p class="footer">Copyright <?php echo date('Y'); ?> &copy; <a href="http://twitter.com/lazysod" target="_NEW">Barry Smith</a></p>
			</footer>    
		</div>
    </div>
    <!-- /.container -->
    <!-- jQuery Version 1.11.1 -->
    <script src="<?php echo $config->urls->templates; ?>/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $config->urls->templates; ?>/js/bootstrap.min.js"></script>

</body>

</html>
