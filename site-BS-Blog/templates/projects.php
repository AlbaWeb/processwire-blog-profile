<?php 
require 'inc/header.php'; 
    if($page->id=='1016'){
        header('location: '.$config->urls->root);
    }

?>

    <!-- Page Content -->
    <div class="container">

        <div class="row row-centered">
            <?php 
            	if($page->h1){
            		echo '<h1 class="text-center">'.$page->greeting.'</h1>';
            	}
            ?>
            <div class="col-lg-8 well text-left">
                
                <!-- start content -->
                <?php 
                	if($page->h1){
                		echo '<h1>'.$page->h1.'</h1>';
                	}
                  if($page->projectName){
                    echo '<p class="lead"><i>'.$page->projectName.'</i></p>';
                  }
                  echo $page->body;
                ?>
            </div>
            <div class="col-md-4" id="leftCol">
                <div id="sidebar">
					<div class="well text-left">
						<h3 class="text-center">Project Links</h3>
					    <div class="list-group">
                  <?php 
                    if($page->projectGit){
                      echo '<a href="'.$page->projectGit.'" target="_NEW" title="Project on Github" class="list-group-item"><i class="fa fa-github-square"></i> Github</a>';
                    }
                    if($page->projectBitbucket){
                      echo '<a href="'.$page->projectBitbucket.'" target="_NEW" title="Project on Bitbucket" class="list-group-item"><i class="fa fa-bitbucket-square"></i> Bitbucket</a>';
                    }
                  ?>               
                  <?php  
                  if($page->projectFacebook){ ?>
                    
                      <a href="<?php echo $page->projectFacebook; ?>" target="_NEW" title="Join me on Facebook" class="list-group-item"><i class="fa fa-facebook-square"></i> Facebook</a>
                    
                  <?php 
                    }
                    if($page->projectUrl){ ?>
                  <!-- Google plus link -->
                    
                      <a href="<?php echo $page->projectUrl; ?>" target="_NEW" title="Project Website" class="list-group-item"><i class="fa fa-globe"></i> <?php echo $page->projectUrl; ?></a>
                    
                  <?php 
                    } 
                  if($page->projectTwitter){ ?>
                  <!-- Twitter link -->
                  
                    <a href="<?php echo $page->projectTwitter; ?>" target="_NEW" title="Follow project on Twitter" class="list-group-item"><i class="fa fa-twitter-square"></i> Project Twitter</a>
                  
                  <?php
                    }
                  if($page->projectDownload){ ?>
                  <!-- instagram link -->
                  
                    <a href="<?php echo $page->projectDownload; ?>" target="_NEW" title="Download now" class="list-group-item"><i class="fa fa-download"></i> Download</a>
                  
                  <?php 
                    }
                    ?>			    	
					    </div>
              <?php 
                if(!$page->projectDownload && !$page->projectTwitter && !$page->projectUrl && !$page->projectFacebook && !$page->projectBitbucket && !$page->projectGit){
                  echo '<p class="lead text-center"><i>There are no links at this moment</i></p>';
                }

              ?>
					</div>
                </div>
            </div>
             
        </div>
        <!-- /.row -->
		<div class="row row-centered">   
			<footer>
		   		<p class="footer">Copyright <?php echo date('Y'); ?> &copy; <a href="http://twitter.com/lazysod" target="_NEW">Barry Smith</a></p>
			</footer>    
		</div>
    </div>
    <!-- /.container -->
    <!-- jQuery Version 1.11.1 -->
    <script src="<?php echo $config->urls->templates; ?>/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $config->urls->templates; ?>/js/bootstrap.min.js"></script>

</body>

</html>
