<?php 
    require 'inc/header.php'; 
    
?>

    <!-- Page Content -->
    <div class="container">
        <div class="row row-centered">
            <div class="col-md-8 col-xs-8 pagination">
                <?php 

                    $limit = 4;
                    
                    if($_GET["page"] == 0){
                      $startNum = 1;
                    }else{
                      $startNum = $_GET["page"];
                    }
                    $start = ($startNum-1) * $limit;

                    if($tag = $sanitizer->selectorValue($input->urlSegment1)) {

                        $name = $sanitizer->pageName($input->urlSegment1);
                        $posts = $pages->find("category*=$tag, start=$start, limit=$limit, template=news-item, sort=date");
                        //
                        $pagination = $posts->renderPager();
                        $badwords = array("<li class='MarkupPagerNavNext MarkupPagerNavLast'><a href='/tk421/blog/www/news/".$sanitizer->selectorValue($input->urlSegment1)."/?page=2'><span>Next</span></a></li>");
                        $pagination = str_replace($badwords, "", $pagination);
                        echo $pagination;                  
                        $results = $posts = $pages->find("category*=$tag, start=$start, limit=$limit, template=news-item, sort=date");      
                        //
                    }else{
                        $posts = $page->find("limit=$limit, template=news-item, sort=date");
                        $pagination = $posts->renderPager();
                        $badwords = array("<li class='MarkupPagerNavNext MarkupPagerNavLast'><a href='/tk421/blog/www/news/?page=2'><span>Next</span></a></li>");
                        $pagination = str_replace($badwords, "", $pagination);
                        echo $pagination;

                        $results = $page->children()->find("start=$start,limit=$limit, sort=-date");  
                    }
                ?>
            </div>

            <div class="col-md-8">
                <?php
                    $count = count($results);
                    if($count > 0){
                        foreach($results as $result){
                            
                            if($result->date){
                                
                                $taglink = $result->category->title;
                                $taglink = strtolower($taglink);
                                $taglink = str_replace(' ', '-', $taglink);

                                echo '<div class="well text-left">';
                                echo '<a href="'.$result->url.'"><h3 class="newsHead">'.$result->title.'</h3></a>';
                                echo '<i class="fa fa-clock-o"></i> Posted on '.$result->date.' Category <a href="'.$taglink.'">'.$result->category->title.'</a>';
                                echo '<hr/>';
                                echo wordLimiter($result->body).'<p><button class="btn btn-primary" onclick="window.location=\''.$result->url.'\'">Read More <i class="fa fa-chevron-right"></i></button></p>';
                                echo '</div>';
                            }
                        }                        
                    }else{

                        echo '<div class="well text-left">';
                        echo '<p class="lead text-center">Sorry! Nothing found</p>'; 
                        echo '</div>';
                    }
                    
                ?>
            </div>
            <div class="col-md-4">
                <div class="col-md-12 well">
                    <h4>Blog Search</h4>
                    <form id="search_form" action="<?php echo $config->urls->root?>search/" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Keywords" name="q" id="search_query" value="<?php echo htmlentities($input->whitelist('q'), ENT_QUOTES, 'UTF-8'); ?>" />
                            <span class="input-group-btn">
                                <button class="btn btn-default button postfix" id="search_submit" type="submit">
                                    <i class="fa fa-search"></i>
                            </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.input-group -->
                </div>
            
                <div class="col-md-12 well text-left well">
                    
                    <h4>Blog Categories</h4>
                    <ul class="list-unstyled catList">
                    <?php
                        $blog = $pages->get("/blog/"); 
                        $overview = $pages->get("/category/"); 
                        $overviewchildren = $overview->children;

                        foreach($overviewchildren as $overviewchild) {
                            $taglink = $overviewchild->title;
                            $taglink = strtolower($taglink);
                            $taglink = str_replace(' ', '-', $taglink);  

                            echo '<li><i class="fa fa-angle-right"></i> <a href="'.$blog->url.$taglink.'">'.$overviewchild->title.'</a></li>';
                                                   
                        }
                    ?>                    
                    </ul>
                </div>
                <?php

                    $widget = $pages->get("/blog/")->widgetRepeat; 
                    foreach($widget as $w){
                        echo '<div class="col-md-12 well">'; 
                        echo $w->widgetBody;
                        echo '</div>'; 
                    }
                ?>
            </div>
        </div>
        <!-- /.row -->
		<div class="row row-centered">   
			<footer>
		   		<p class="footer">Copyright <?php echo date('Y'); ?> &copy; <a href="http://twitter.com/lazysod" target="_NEW">Barry Smith</a></p>
			</footer>    
		</div>
    </div>
    <!-- /.container -->
    <!-- jQuery Version 1.11.1 -->
    <script src="<?php echo $config->urls->templates; ?>/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $config->urls->templates; ?>/js/bootstrap.min.js"></script>

</body>

</html>
