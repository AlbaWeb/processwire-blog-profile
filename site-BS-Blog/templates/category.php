<?php require 'inc/header.php'; 
    


?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <?php 
               
                echo '<ul class="breadcrumb">';
                foreach($page->parents()->append($page) as $parent) {
                    echo "<li><a href='{$parent->url}'>{$parent->title}</a></li>";
                }
                echo '</ul>';
            ?>
        </div>
        <div class="row row-centered">
            <div class="col-lg-12 col-md-12  col-xs-12 col-centered well">
                
                <!-- start content -->
                
                <?php 
                    if($page->h1){
                        echo '<h1>'.$page->h1.'</h1>';
                    }else{
                        echo '<h1>'.$page->title.'</h1>';
                    }
                    echo '<span class="lead"><i>Posted '.$page->date.'</i></span>'; 
                    echo '<hr/>';
                	echo $page->body;
                ?>
                </div>
            </div> 
        </div>
        <!-- /.row -->
		<div class="row row-centered">   
			<footer>
		   		<p class="footer">Copyright <?php echo date('Y'); ?> &copy; <a href="http://twitter.com/lazysod" target="_NEW">Barry Smith</a></p>
			</footer>    
		</div>
    </div>
    <!-- /.container -->
    <!-- jQuery Version 1.11.1 -->
    <script src="<?php echo $config->urls->templates; ?>/js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $config->urls->templates; ?>/js/bootstrap.min.js"></script>

</body>

</html>
