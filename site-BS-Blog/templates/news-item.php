<?php require 'inc/header.php'; 
    


?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <?php 
               
                echo '<ul class="breadcrumb">';
                foreach($page->parents()->append($page) as $parent) {
                    echo "<li><a href='{$parent->url}'>{$parent->title}</a></li>";
                }
                echo '</ul>';
            ?>
        </div>
        <div class="row">
            <?php
                if(count($page->image)>0){
                    echo '<div class="col-lg-12 well">'; 
                        foreach($page->image as $image) {
                            $thumbnail = $image->size(120,120);
                            //<a class="fancybox" href="3_b.jpg" data-fancybox-group="gallery" title="Cras neque mi, semper leon"><img src="3_s.jpg" alt=""></a>
                           echo '<div class="col-md-1"><a href="'.$image->url.'" class="fancybox" data-fancybox-group="gallery"><img src="'.$thumbnail->url.'" alt="'.$thumbnail->description.'" class="img-responsive"></a></div>';
                        } 

                    echo '</div>';
                }

    
            
            ?>
                
            
        </div>
        <div class="row row-centered">
            <div class="col-lg-12 col-md-12  col-xs-12 col-centered well">
                <!-- start content -->
                
                <?php 
                    if($page->h1){
                        echo '<h1>'.$page->h1.'</h1>';
                    }else{
                        echo '<h1>'.$page->title.'</h1>';
                    }
                    $taglink = $page->category->title;
                    $taglink = strtolower($taglink);
                    $taglink = str_replace(' ', '-', $taglink);
                    
                    echo '<span class="lead">Category <a href="../'.$taglink.'">'.$page->category->title.'</a> <i>Posted '.$page->date.'</i></span>'; 
                    echo '<hr/>';
                	echo $page->body;
                ?>
                </div>
            </div> 
        </div>
        <!-- /.row -->
		<div class="row row-centered">   
			<footer>
		   		<p class="footer">Copyright <?php echo date('Y'); ?> &copy; <a href="http://twitter.com/lazysod" target="_NEW">Barry Smith</a></p>
			</footer>    
		</div>
    </div>
    <!-- /.container -->
    <?php 
    
    if($page->parent_id!=1021){ ?>
    <!-- jQuery Version 1.11.1 -->
    <script src="<?php echo $config->urls->templates; ?>/js/jquery.js"></script>
    <?php } ?>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo $config->urls->templates; ?>/js/bootstrap.min.js"></script>

</body>

</html>
