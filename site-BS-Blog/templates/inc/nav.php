<?php 
function renderChildrenOf($pa, $ignoreParents = null, $ignoreSubs = null, $root = null, $output = '', $level = 0) {
    //vars
    $ignoreSubnav = (isset($ignoreSubs) ? $ignoreSubs : array());
    $ignoreParents = (isset($ignoreParents) ? $ignoreParents : array());
    $output = '';
    $level++;

    if(!$root) $root = wire("pages")->get(1);    
    
    $homeclass = (wire("page")->id == $root ? 'class="active"' : false );
    //$output .= ($level == 1 ? "<li $homeclass><a href='/'>Home</a></li>" : false);

    foreach($pa as $child) {
        $atoggle = '';
        $class = '';
        $carret = '';
        $has_children = count($child->children) ? true : false;
 
        if($has_children && $child !== $root && !in_array($child->id, $ignoreSubnav)) {
            if($level == 1){
                $class .= 'dropdown'; // first level boostrap dropdown li class
                $carret = ' <b class="caret"></b>';
                $atoggle .= ' class="dropdown-toggle" data-toggle="dropdown"'; // first level anchor attributes
            } else {
                $class .= 'dropdown-submenu'; // sub level boostrap dropdown li class
            }
        }
 
        // make the current page and only its first level parent have an active class
        if($child === wire("page")){
            $class .= ' active';
        } else if($level == 1 && $child !== $root){
            if($child === wire("page")->rootParent || wire("page")->parents->has($child)){
                $class .= ' active';
            }
        }
 
        $class = strlen($class) ? " class='".trim($class)."'" : '';
        if (!in_array($child->id, $ignoreParents)) {
            $output .= "<li$class$hidden><a href='$child->url'$atoggle>$child->title$carret</a>";
        }
 
        // If this child is itself a parent and not the root page, then render its children in their own menu too...
        if($has_children && $child !== $root && !in_array($child->id, $ignoreSubnav)) {
            $output .= renderChildrenOf($child->children, $ignoreParents, $ignoreSubnav, $root, $output, $level);
        }
        $output .= '</li>';
    }
    $outerclass = ($level == 1) ? "nav navbar-nav" : 'dropdown-menu';
    return "<ul class='$outerclass navbar-right'>$output</ul>";
}
 
// bundle up the first level pages and prepend the root home page
$root = $pages->get(1);
$pa = $root->children;
/*ignore subnav for pages by page->id
* e.g. $ignoreSubnav = array('1010', '1021', '3345');
*/
$ignoreParents = array('1');
$ignoreSubnav = array('1001', '1021');
if($root->blogControl->title=='inactive'){
    
    $results = $page->find("template=news-item");
    foreach($results as $result){
        $newNum = $result->parentID;
        $ignoreParents[] = $newNum;
    }
}
//var_dump($ignoreParents);
// Set the ball rolling...
echo renderChildrenOf($pa,$ignoreParents,$ignoreSubnav); ?>